package com.zj.exercise1.app;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.zj.exercise1.app.adapters.FruitAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ManageFruitsTask fruitsTask;
    private FruitAdapter adapter;
    private RecyclerView recyclerView;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startButton = (Button) findViewById(R.id.start_btn);
        startButton.setOnClickListener(this);
        Button stopButton = (Button) findViewById(R.id.stop_btn);
        stopButton.setOnClickListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        adapter = new FruitAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void showList() {
        findViewById(R.id.empty_list_info).setVisibility(View.GONE);
        findViewById(R.id.recycler_view).setVisibility(View.VISIBLE);
    }

    private void hideList() {
        findViewById(R.id.recycler_view).setVisibility(View.GONE);
        findViewById(R.id.empty_list_info).setVisibility(View.VISIBLE);
    }

    private void updateFruits() {
        adapter.updateFruitList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_btn:
                if (fruitsTask == null || fruitsTask.isCancelled()) {
                    fruitsTask = new ManageFruitsTask();
                    fruitsTask.execute();
                    showList();
                }
                break;
            case R.id.stop_btn:
                if (fruitsTask != null)
                    if(!fruitsTask.isCancelled()) {
                        fruitsTask.cancel(true);
                    } else {
                        adapter.removeFruits();
                        hideList();
                    }
                break;
        }
    }

    private class ManageFruitsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (!isCancelled()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    return null;
                }
                publishProgress();
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            updateFruits();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_list_layout) {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            item.setVisible(false);
            menu.findItem(R.id.action_grid_layout).setVisible(true);
            return true;
        }
        if (id == R.id.action_grid_layout) {
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            item.setVisible(false);
            menu.findItem(R.id.action_list_layout).setVisible(true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(fruitsTask != null && !fruitsTask.isCancelled()) {
            fruitsTask.cancel(true);
        }
    }
}
