package com.zj.exercise1.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.zj.exercise1.app.R;
import com.zj.exercise1.app.model.Apple;
import com.zj.exercise1.app.model.Fruit;
import com.zj.exercise1.app.model.Orange;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FruitAdapter extends RecyclerView.Adapter<FruitAdapter.FruitViewHolder> {

    private List<Fruit> fruitList;
    private Random generator;

    public FruitAdapter() {
        this.fruitList = new ArrayList<Fruit>();
        generator = new Random();
    }

    @Override
    public FruitViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_view, viewGroup, false);

        return new FruitViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FruitViewHolder holder, int position) {
        holder.position = position;
        holder.fruitName.setText(fruitList.get(position).getName());
        holder.fruitCounter.setText(String.valueOf(fruitList.get(position).getCounter()));
        holder.fruitLayout.setBackgroundColor(fruitList.get(position).getColor());
    }

    @Override
    public int getItemCount() {
        return fruitList.size();
    }

    public void updateFruitList() {
        if (fruitList.size() < 10) {
            fruitList.add(generator.nextBoolean() ? new Apple() : new Orange());
        } else {
            if (generator.nextInt(100) < 10) {
                fruitList.remove(generator.nextInt(fruitList.size()));
            } else {
                fruitList.get(generator.nextInt(fruitList.size())).increaseCounter();
            }
        }
        notifyDataSetChanged();
    }

    public void removeFruits() {
        fruitList.clear();
        notifyDataSetChanged();
    }

    public class FruitViewHolder extends RecyclerView.ViewHolder {

        int position;
        RelativeLayout fruitLayout;
        TextView fruitName;
        TextView fruitCounter;

        public FruitViewHolder(View itemView) {
            super(itemView);
            fruitLayout = (RelativeLayout) itemView;
            fruitName = (TextView) itemView.findViewById(R.id.fruit_name);
            fruitCounter = (TextView) itemView.findViewById(R.id.fruit_counter);
        }
    }
}
