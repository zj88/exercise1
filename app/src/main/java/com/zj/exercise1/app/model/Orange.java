package com.zj.exercise1.app.model;

import android.graphics.Color;

public class Orange extends Fruit{

    public Orange() {
        super();
        name = "Pomarańcza";
        color = Color.parseColor("#FFA500");
    }

    @Override
    public int getCounter() {
        return super.getCounter() * 11;
    }
}
