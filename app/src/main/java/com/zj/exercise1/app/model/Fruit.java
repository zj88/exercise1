package com.zj.exercise1.app.model;

public class Fruit {

    protected String name;
    protected int color;
    protected int counter;

    public Fruit() {
        counter = 1;
    }

    public String getName() {
        return name;
    }

    public int getColor() {
        return color;
    }

    public int getCounter() {
        return counter;
    }

    public void increaseCounter() {
        counter++;
    }
}
