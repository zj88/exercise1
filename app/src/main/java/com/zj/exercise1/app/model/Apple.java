package com.zj.exercise1.app.model;

import android.graphics.Color;

public class Apple extends Fruit {

    public Apple() {
        super();
        name = "Jabłko";
        color = Color.parseColor("#A4C639");
    }
}
